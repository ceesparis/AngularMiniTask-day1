export interface Podcast {
  title: string;
  name: string;
  image: string;
}