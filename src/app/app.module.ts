import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PodcastsPage } from './pages/podcasts/podcasts.page';
import { PodcastGenresPage } from './pages/podcast-genres/podcast-genres.page';
import { PodcastsDirectoryComponent } from './components/podcasts-directory/podcasts-directory.component';

@NgModule({
  declarations: [
    AppComponent,
    PodcastsPage,
    PodcastGenresPage,
    PodcastsDirectoryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
