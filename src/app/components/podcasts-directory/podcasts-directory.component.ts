import { Component, Input, OnInit } from '@angular/core';
import { Podcast } from '../../models/podcast.model'


@Component({
  selector: 'app-podcasts-directory',
  templateUrl: './podcasts-directory.component.html',
  styleUrls: ['./podcasts-directory.component.scss']
})
export class PodcastsDirectoryComponent implements OnInit {

  @Input()
  podcasts: Podcast[] = []; 

  constructor() { }

  ngOnInit(): void {
  }

}
