import { Component, OnInit } from '@angular/core';

class Podcast {
  title: string;
  name: string;
  image: string;
  constructor(title: string, name: string, image: string){
    this.title = title;
    this.name = name;
    this.image = image
  }
}
@Component({
  selector: 'app-podcasts-page',
  templateUrl: './podcasts.page.html',
  styleUrls: ['./podcasts.page.scss']
})
export class PodcastsPage implements OnInit {

  podcast1 = new Podcast("crazy stories", "Cornel Razy", "assets/image/default_picture.jpeg")
  podcast2 = new Podcast("Politricks", "Raymond Uin", "assets/image/politics.jpg")
  podcast3 = new Podcast("Music: How to Make It", "Stevie Ray Wonder", "assets/image/music.jpg")
  podcast4 = new Podcast("What the Heck is the Blockhain?", "Chris Currency", "assets/image/blockchain.jpg")
  podcast5 = new Podcast("The Mysterious Death of Gareth Gutter", "Carlos Contrived", "assets/image/mystery.jpg")
  podcast6 = new Podcast("IT: how to make it", "Bill Binary", "assets/image/coding.jpg")
  podcast7 = new Podcast("Lessons from Hackerwoman", "Susan Hacker", "assets/image/default_picture.jpeg")
  podcast8 = new Podcast("The Presocratics", "Lily Skeptic", "assets/image/susanSketpic.jpg")
  podcast9 = new Podcast("How to deal with Cocktail Parties", "Sandy Ocialite", "assets/image/cocktail.jpg")
  podcast10 = new Podcast("History: what to Learn from it", "Hal Hindsight", "assets/image/history.jpg")


  podcasts = [
  this.podcast1, this.podcast2, this.podcast3,
  this.podcast4, this.podcast5, this.podcast6,
  this.podcast7, this.podcast8, this.podcast9,
  this.podcast10
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
